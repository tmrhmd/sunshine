import json
import datetime
import tkinter.font
import urllib.request
import tkinter as gui
import tkinter.messagebox


class Application(gui.Frame):
    def __init__(self, master=None):
        gui.Frame.__init__(self, master=master, padx=50, pady=20)
        self.pack()
        self.set_variables()
        self.build_ui()

    def set_variables(self):
        self.zip_code = gui.StringVar(self, '')
        self.zip_code.trace('w', lambda x, y, z: self.validate_zip_len())
        self.zip_code.trace('w', lambda x, y, z: self.validate_zip_type())
        self.zip_code.trace('w', lambda x, y, z: self.validate_zip())

    def build_ui(self):
        # Program name
        program_name = gui.Label(self, text='Sunshine!', font=tkinter.font.Font(self, size=15))
        program_name.grid(row=0, column=0, sticky=gui.W + gui.E, pady=15)

        # Program name
        zip_label = gui.Label(self, text='Enter a U.S. Zip code')
        zip_label.grid(row=1, column=0, sticky=gui.W + gui.E, pady=5)

        # ZIP code input
        zip_code_input = gui.Entry(self, textvariable=self.zip_code)
        zip_code_input.grid(row=2, column=0, sticky=gui.W + gui.E, pady=10)

        # Button
        self.check_button = gui.Button(self, text='Check', state='disabled', command=lambda: self.display_weather())
        self.check_button.grid(row=3, column=0, sticky=gui.W + gui.E)

    def validate_zip_type(self):
        """ Limit to digits """
        if self.zip_code.get().isdigit() and self.zip_code.get() != '':
            return True
        self.zip_code.set(self.zip_code.get()[:-1])
        return False

    def validate_zip_len(self):
        """ Limit to 5 digits """
        if len(self.zip_code.get()) > 5:
            self.zip_code.set(self.zip_code.get()[:-1])
            return False
        return True

    def validate_zip(self):
        if len(self.zip_code.get()) >= 5 and self.zip_code.get().isdigit():
            self.check_button['state'] = 'normal'
        else:
            self.check_button['state'] = 'disabled'

    def fetch_weather(self):
        """ Fetch weather JSON from OWM """
        request = 'http://api.openweathermap.org/data/2.5/weather?zip=' + self.zip_code.get() + ',us&APPID=a52853c3904ef4296406be0966433a1f'

        response = None
        try:
            response = urllib.request.urlopen(request)
        except Exception:
            self.show_prompt('error', 'Couldn\'t get forecast data!')
            return None

        forecast = json.loads(response.read().decode('utf-8'))
        if forecast['cod'] == 200:
            city = forecast['name']
            temp = forecast['main']['temp']
            humidity = forecast['main']['humidity']
            description = forecast['weather'][0]['description']
            time = self.utc2local(forecast['dt']).strftime("%A") + ' ' + self.utc2local(forecast['dt']).strftime(
                '%H:%M')
            sunrise = self.utc2local(forecast['sys']['sunrise']).strftime('%H:%M')
            sunset = self.utc2local(forecast['sys']['sunset']).strftime('%H:%M')
            return {'city': city, 'temp': temp, 'description': description, 'time': time, 'sunrise': sunrise, 'sunset': sunset,
                    'humidity': humidity}
        else:
            self.show_prompt('error', 'Couldn\'t get forecast info!')

    def rebuild_ui(self):
        forecast = self.fetch_weather()

        self.temp_label = gui.Message(self, text=str(self.k2f(forecast['temp'])) + u'\N{DEGREE SIGN}' + 'F', font=tkinter.font.Font(self, family='Helvetica', size=30), width=150)
        self.temp_label.grid(row=0, column=0, sticky=gui.W + gui.E)

        self.to_celsius = gui.Button(self, text='C' + u'\N{DEGREE SIGN}', command=lambda: self.convert_temp('c', forecast['temp']))
        self.to_celsius.grid(row=1, column=0, sticky=gui.E, ipadx=15, pady=15)
        self.to_fahrenheit = gui.Button(self, text='F' + u'\N{DEGREE SIGN}', state='active', command=lambda: self.convert_temp('f', forecast['temp']))
        self.to_fahrenheit.grid(row=1, column=0, sticky=gui.W, ipadx=15, pady=15)

        city_label = gui.Label(self, text=forecast.get('city'))
        city_label.grid(row=3, column=0, sticky=gui.E+gui.W)
        time_label = gui.Label(self, text=forecast.get('time'))
        time_label.grid(row=4, column=0, sticky=gui.E+gui.W)
        description_label = gui.Label(self, text=forecast.get('description').capitalize())
        description_label.grid(row=5, column=0, sticky=gui.E+gui.W)

    def display_weather(self):
        if self.validate_zip_type() and self.validate_zip_len():
            for w in self.winfo_children():
                w.destroy()
        self.rebuild_ui()

    def show_prompt(self, t, m):
        if t == 'info':
            tkinter.messagebox.showinfo(t, m)
        elif t == 'warning':
            tkinter.messagebox.showwarning(t, m)
        elif t == 'error':
            tkinter.messagebox.showerror(t, m)
        else:
            tkinter.messagebox.showinfo(t, m)

    def convert_temp(self, d, t):
        if d == 'c':
            self.temp_label['text'] = str(self.k2c(t)) + u'\N{DEGREE SIGN}' + 'C'
        elif d == 'f':
            self.temp_label['text'] = str(self.k2f(t)) + u'\N{DEGREE SIGN}' + 'F'
        else:
            return None

    @staticmethod
    def k2f(k):
        return int(k * (9 / 5) - 459.67)

    @staticmethod
    def k2c(k):
        return int(k - 273.15)

    @staticmethod
    def utc2local(t):
        return datetime.datetime.fromtimestamp(t)

root = gui.Tk()
root.wm_title('Sunshine!')
root.resizable(height=False, width=False)

app = Application(root)
app.mainloop()
